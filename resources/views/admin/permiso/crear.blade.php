@extends("theme.$theme.layout")

@section('titulo')
    Permisos
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">Permisos</h3>
                    </div>
                    <div class="card-body table-responsive p-0">
                        Aqui el formulario
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection