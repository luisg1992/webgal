@extends("theme.$theme.layout")

@section('titulo')
    Sistema Menús
@endsection

@section('scripts')
    <script src="{{asset("assets/pages/scripts/admin/menu/crear.js")}}"></script>
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('includes.form-error')
                @include('includes.mensaje')
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Crear Menús</h3>
                    </div>
                    <form class="form-horizontal" action="{{route('guardar_menu')}}" id="form-general" method="POST" autocomplete="off">
                        @csrf
                        <div class="card-body">
                            @include('admin.menu.form')
                        </div>
                        <div class="card-footer row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                @include('includes.boton-form-crear')
                            </div>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection