<div class="form-group row">
    <label for="nombre" class="col-lg-3 col-form-label requerido">Nombre</label>
    <div class="col-lg-8">
      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" required value="{{old('nombre')}}" />
</div>
</div>
  <div class="form-group row">
    <label for="url" class="col-lg-3 col-form-label requerido">Url</label>
    <div class="col-lg-8">
      <input type="text" name="url" class="form-control" id="url" placeholder="url"  required value="{{old('url')}}" />
    </div>
 </div>
 <div class="form-group row">
    <label for="icono" class="col-lg-3 col-form-label">Icono</label>
    <div class="col-lg-8">
      <input type="text" name="icono" class="form-control" id="icono" placeholder="icono" value=""/>
    </div>
    <div class="col-lg-1">
      <span id="mostrar-icono" class="fa fa-fw {{old("icono")}}"></span>
    </div>
  </div>
