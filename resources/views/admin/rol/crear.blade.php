@extends("theme.$theme.layout")
@section('titulo')
    Roles
@endsection

@section('scripts')
    <script src="{{asset("assets/pages/scripts/admin/crear.js")}}"></script>
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('includes.form-error')
                @include('includes.mensaje')
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Crear Roles</h3>
                        <div class="card-tools">
                            <a href="{{route('rol')}}" class="btn btn-block btn-info btn-sm">
                                <i class="fa fa-fw fa-reply-all"></i>Volver al listado
                            </a>
                          </div>
                    </div>
                    <form class="form-horizontal" action="{{route('guardar_rol')}}" id="form-general" method="POST" autocomplete="off">
                        @csrf
                        <div class="card-body">
                            @include('admin.rol.form')
                        </div>
                        <div class="card-footer row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                @include('includes.boton-form-crear')
                            </div>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection