@extends("theme.$theme.layout")
@section('titulo')
Roles
@endsection

@section('scripts')
    <script src="{{asset("assets/pages/scripts/admin/index.js")}}"></script>
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('includes.mensaje')
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Roles</h3>
                        <div class="card-tools">
                            <a href="{{route('crear_rol')}}" class="btn btn-block btn-success btn-sm">
                                <i class="fa fa-fw fa-plus-circle"></i>Nuevo Registro
                            </a>
                          </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped table-bordered table-hover" id="tabla-data">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th class="width70"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{$data->nombre}}</td>
                                        <td>
                                            <a href="{{route('editar_rol', ['id'=>$data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                                <i class="fa fa-fw fa-file"></i>
                                            </a>
                                            <form action="{{route('eliminar_rol', ['id'=>$data->id])}}" class="d-inline form-eliminar" method="POST">
                                                @csrf @method("delete")
                                                <button type="submit" class="btn-accion-tabla eliminar toolttipsC" title="Eliminar este registro">
                                                    <i class="fa fa-fw fa-trash text-danger"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection